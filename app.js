const scrapeCategoryHierarchy = require("./services/scrape-category-hierarchy");
const scrapeAllProductsData = require("./services/scrape-all-products-data");
const generateProductData = require("./services/generate-product-data");
const generateCategoriesData = require("./services/generate-categories-data");

(async () => {
  console.warn("Scraping product info...\nPlease wait");
  
  const categories = await scrapeCategoryHierarchy();
  const allProductsData = await scrapeAllProductsData();

  generateCategoriesData(categories);
  generateProductData(allProductsData);
})();
