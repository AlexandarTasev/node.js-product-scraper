const puppeteer = require("puppeteer");
const { baseURL } = require("../utils/constants");

const scrapeCategoryHierarchy = async () => {
  try {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    await page.goto(baseURL);
    await page.waitForSelector(".cat-item");

    const categoriesList = await page.evaluate(() =>
      Array.from(document.querySelectorAll(".product-categories > li")).map(
        (category) => ({
          mainCategory: category.querySelector(".cat-item a").innerText,
          subCategories: Array.from(
            category.querySelectorAll(".cat-item ul a")
          ).map((subcategory) => subcategory.innerText),
        })
      )
    );

    browser.close();

    return categoriesList;
  } catch (error) {
    console.log(error);
  }
};

module.exports = scrapeCategoryHierarchy;
