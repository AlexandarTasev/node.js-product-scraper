const puppeteer = require("puppeteer");
const scrapeProductDetails = require("./scrape-product-details");
const { baseURL } = require("../utils/constants");

const scrapeAllProductsData = async () => {
  const browser = await puppeteer.launch();

  const extractProducts = async (url) => {
    const page = await browser.newPage();
    await page.goto(url);
    await page.waitForSelector(".cat-item");

    const productsOnPage = await page.evaluate(() =>
      Array.from(document.querySelectorAll(".columns-4 > li")).map(
        (detectedProduct) => ({
          detailsPage: detectedProduct.querySelector("a").href,
        })
      )
    );

    // Recursively scrape the next page
    if (nextPage) {
      // Fetch product from next page
      pageNumber++;
      const nextUrl = `${baseURL}page/${pageNumber}`;

      nextPage = await page.evaluate(() =>
        document.querySelector(".page-numbers > next") ? true : false
      );

      return productsOnPage.concat(await extractProducts(nextUrl));
    } else {
      // Terminate if there are no more pages
      return productsOnPage;
    }
  };

  let nextPage = true;
  let pageNumber = 1;
  const firstUrl = baseURL;
  const allProducts = await extractProducts(firstUrl);
  await browser.close();

  return scrapeProductDetails(allProducts);
};

module.exports = scrapeAllProductsData;
