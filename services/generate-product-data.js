const fs = require("fs-extra");

const generateProductData = (products) => {
  const fileLocation = "./data/products.csv";

  fs.writeFile(
    `${fileLocation}`,
    "Product name, Product image URL, SKU, Product category, Product description, Product color option, Product size option, Product logo option, Product price, Product attributes,\n"
  );

  products.forEach((product) => {
    fs.appendFile(
      `${fileLocation}`,
      `"${product.title}","${product.imageUrl}","${product.sku}","${product.category}","${product.description}","${product.color}","${product.size}", "${product.logo}", "${product.price}", "${product.attributes}"\n`
    );
  });

  console.log(
    `Scraping of products completed successfully!\nData is available at: ${fileLocation}`
  );
};

module.exports = generateProductData;
