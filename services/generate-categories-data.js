const fs = require("fs-extra");

const generateCategoriesData = (data) => {
  const fileLocation = "./data/categories.csv";

  fs.writeFile(`${fileLocation}`, "Main Category, Sub Categories\n");

  data.forEach((element) => {
    fs.appendFile(
      `${fileLocation}`,
      `"${element.mainCategory}","${element.subCategories}"\n`
    );
  });

  console.log(`Scraping of categories completed successfully!\nData is available at: ${fileLocation}`)
};

module.exports = generateCategoriesData;
