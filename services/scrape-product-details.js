const puppeteer = require("puppeteer");

const scrapeProductDetails = async (productsOnPage) => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  const productsList = [];

  for (const product of productsOnPage) {
    const sizeOptions = [];
    const colorOptions = [];
    const logoOptions = [];
    const attributesDescription = [];

    await page.goto(product.detailsPage);
    await page.waitForSelector("img");

    await page.evaluate(() => {
      colorOptions = Array.from(
        document.querySelectorAll("#pa_color option")
      )?.map((option) => option.innerText);
      colorOptions.shift();
    });

    await page.evaluate(() => {
      sizeOptions = Array.from(
        document.querySelectorAll("#pa_size option")
      )?.map((option) => option.innerText);
      sizeOptions.shift();
    });

    await page.evaluate(() => {
      logoOptions = Array.from(document.querySelectorAll("#logo option"))?.map(
        (option) => option.innerText
      );
      logoOptions.shift();
    });

    await page.evaluate(() => {
      attributesDescription = Array.from(
        document.querySelectorAll(".shop_attributes p")
      )?.map((attr) => attr.innerText);
    });

    const productDetails = await page.evaluate(() => ({
      title: document.querySelector(".product_title").innerText,
      imageUrl: document.querySelector(
        ".woocommerce-product-gallery__image > img"
      )?.src,
      sku: document.querySelector(".sku_wrapper").innerText,
      category: document.querySelector(".posted_in").innerText,
      description: document.querySelector("#tab-description p").innerText,
      color: colorOptions,
      size: sizeOptions,
      logo: logoOptions,
      price: document.querySelector(".price > span").innerText,
      attributes: attributesDescription,
    }));

    productsList.push(productDetails);
  }

  await browser.close();

  return productsList;
};

module.exports = scrapeProductDetails;
