# Node.js Product Scraper
Scraping Node.js application for scraping product listing data from an eRetailer web site. The data is prepared for processing in CSV files as an output of the scripts.

## Resolve Dependencies
```
npm / yarn install
```
## Run the application
To run the app use:
```
npm start
```
or
```
npm run dev
```
to start it with `nodemon`

